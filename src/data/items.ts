import { Item } from "../types/Item";

export const items: Item[] = [
  {
    date: new Date(2021, 9, 15),
    category: 'food',
    title: 'Subway',
    value: 32.15
  },
  {
    date: new Date(2021, 9, 15),
    category: 'food',
    title: 'American Chicken',
    value: 48.80
  },
  {
    date: new Date(2021, 9, 16),
    category: 'rent',
    title: 'Aluguel APTO',
    value: 1000.00
  },
  {
    date: new Date(2021, 10, 15),
    category: 'salary',
    title: 'Salário Company',
    value: 6400.90
  },
];
